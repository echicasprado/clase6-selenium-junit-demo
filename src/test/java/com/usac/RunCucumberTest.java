package com.usac;

import org.junit.runner.RunWith;
import cucumber.api.junit.Cucumber;
import cucumber.api.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions( format = {"pretty","json:target/cucumber.json"})
public class RunCucumberTest {
}